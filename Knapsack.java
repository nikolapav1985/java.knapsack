    //This is the java program to implement the knapsack problem using Dynamic Programming

    public class Knapsack 

    {

        static int max(int a, int b) 

        { 

            return (a > b)? a : b; 

        }

        static int knapSack(int W, int wt[], int val[], int n)

        {

            int i, w;

            int [][]K = new int[n+1][W+1];

     

    	   // Build table K[][] in bottom up manner

            for (i = 0; i <= n; i++)

            {

                for (w = 0; w <= W; w++)

                {

                    if (i==0 || w==0)

                        K[i][w] = 0;

                    else if (wt[i-1] <= w)

                        K[i][w] = max(val[i-1] + K[i-1][w-wt[i-1]],  K[i-1][w]);

                    else

                        K[i][w] = K[i-1][w];

                }

            }

     

            return K[n][W];

        }

     

        public static void main(String args[])

        {

            int n = 16; // number of items
            int []wt = {1,4,6,2,5,10,8,3,9,1,4,2,5,8,9,1}; // item mass
            int []val = {10,5,30,8,12,30,50,10,2,10,40,80,100,25,10,5}; // item value
            int W = 20; // knapsack capacity

            System.out.println("The maximum value that can be put in a knapsack of capacity W is: " + knapSack(W, wt, val, n));
        }

    }
